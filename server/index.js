
//---------- Constant Variables ----------
const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const mysql = require('mysql')
const cors = require('cors')
const jwt = require('jsonwebtoken')
const dotenv = require("dotenv");

dotenv.config();

//---------- JWT Authentication Verification ----------
function verify(req,res,next) {
    const authHeader =  req.headers.token;   
    if(authHeader){
        const token = authHeader.split(" ")[1]
        jwt.verify(token,'secretkey123456',(err,user)=>{
            if(err) res.status(403).json("Token is not valid")
            req.user =user;
            next()
        })
    }else{
        return res.status(401).json('You are not authenticated');
    }
}
// host: 'localhost',
// user: 'root',
// password: '',
// database: 'nsestock'

//---------- Database Connection ----------
// connectionLimit: 10,
const db = mysql.createPool({
    
    host: 'sql6.freemysqlhosting.net',
    user: 'sql6429159',
    password: 'MywdAawppJ',
    database: 'sql6429159'
});

//---------- Middle Wares ----------
app.use(express.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));



//---------- GET APIs ----------
// Get Stock details filter by Name and ID
app.get('/api/get', verify , (req, res) => {   
    const sqlSelect = "SELECT SNo AS id,Name FROM stocks"
    db.query(sqlSelect, (err, result) => {
        if (err) console.log(err);
        res.status(200).json(result)       
    })
})

// Get Stock details using name
app.get('/api/getDetails/:name', verify ,(req, res) => {   
    const name = req.params.name;
    const sqlSelect = "SELECT * FROM stocks where Name= ?";
    db.query(sqlSelect, name, (err, result) => {
        if (err) console.log(err);
        res.status(200).json(result)       
    })
})

//---------- POST API ----------
// Authentication Login 
app.post('/api/auth/login', (req, res) => {   
    const username = req.body.username;
    const password = req.body.password;
    if (username === "Batman" && password === "iambatman") {
        const accessToken =jwt.sign({username:username},'secretkey123456',{expiresIn:"1d"})        
        res.status(200).json({username,accessToken})
    } else {
        res.status(401).json("invalid username or password")
    }
})

//---------- PORT Settings ----------
app.listen(process.env.PORT || 3000, function(){
    console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env);
  });