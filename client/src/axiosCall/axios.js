import axios from "axios";
// import {BASE_URL} from '../constants/constants'

//--------------- AXIOS instance ---------------
const BASE_URL = "https://riafy-stocks.herokuapp.com/api/";

const instance = axios.create({
  baseURL: BASE_URL,
  headers: {
    token:
      "Bearer " + JSON.parse(localStorage.getItem("user"))?.accessToken || "",
  },
});

// const instance= axios;
// Alter defaults after instance has been created

export default instance;
