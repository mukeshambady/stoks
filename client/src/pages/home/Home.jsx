import './home.css'
import Topbar from "../../component/topbar/Topbar";
import Body from "../../component/body/Body"


function Home() {
  return (
    <>
      <Topbar />
      <Body />
    </>
  );
}

export default Home;


