import "./topbar.css";
import { Logout } from "../../context/AuthActions";
import { AuthContext } from "../../context/AuthContext";
import { useContext } from "react";

export default function Topbar() {
  const { dispatch } = useContext(AuthContext);

  const handleClick = () => {
    dispatch(Logout());
    // localStorage.setItem("user", JSON.stringify(''));
  };

  return (
    <div className="topbar">
      <div className="topbarWrapper">
        <div className="topLeft">
          <span className="logo">Stokes</span>
        </div>
        <div className="topbarRight">
          <button onClick={handleClick}>Logout</button>
        </div>
      </div>
    </div>
  );
}
