import { Grid } from "@material-ui/core";
import "./stockdetails.css";

function StockDetails({ stockDetails, ...props }) {
  return (
    <div className="details">
      <div className="subtitle">{stockDetails[0]?.Name}</div>

      <Grid container>
        <Grid item md={4}>
          <div className="card">
            <div className="item">
              <span>Market Cap</span>
              <span className="item-color">₹{stockDetails[0]?.MarketCap}</span>
            </div>
          </div>
        </Grid>
        <Grid item md={4}>
          <div className="card">
            <div className="item">
              <span>Dividend Yield</span>
              <span className="item-color">
                ₹{stockDetails[0]?.DividendYield}
              </span>
            </div>
          </div>
        </Grid>
        <Grid item md={4}>
          <div className="card">
            <div className="item">
              <span>Debt Equality</span>
              <span className="item-color">
                ₹{stockDetails[0]?.DebtToEquity}
              </span>
            </div>
          </div>
        </Grid>
        <Grid item md={4}>
          <div className="card card-bg">
            <div className="item">
              <span>Current Price</span>
              <span className="item-color">
                ₹{stockDetails[0]?.CurrentMarketPrice}
              </span>
            </div>
          </div>
        </Grid>
        <Grid item md={4}>
          <div className="card card-bg">
            <div className="item">
              <span>ROCE</span>
              <span className="item-color">₹{stockDetails[0]?.ROCEper}</span>
            </div>
          </div>
        </Grid>
        <Grid item md={4}>
          <div className="card card-bg">
            <div className="item">
              <span>EPS</span>
              <span className="item-color">₹{stockDetails[0]?.EPS}</span>
            </div>
          </div>
        </Grid>
        <Grid item md={4}>
          <div className="card ">
            <div className="item">
              <span>Stock P/E</span>
              <span className="item-color">₹{stockDetails[0]?.StockPE}</span>
            </div>
          </div>
        </Grid>
        <Grid item md={4}>
          <div className="card">
            <div className="item">
              <span>ROE</span>
              <span className="item-color">
                ₹{stockDetails[0]?.ROEPreviousAnnum}
              </span>
            </div>
          </div>
        </Grid>
        <Grid item md={4}>
          <div className="card">
            <div className="item">
              <span>Reserves</span>
              <span className="item-color">₹{stockDetails[0]?.Reserves}</span>
            </div>
          </div>
        </Grid>
        <Grid item md={4}>
          <div className="card card-bg">
            <div className="item">
              <span>Debt</span>
              <span className="item-color">₹{stockDetails[0]?.Debt}</span>
            </div>
          </div>
        </Grid>
      </Grid>
    </div>
  );
}

export default StockDetails;
