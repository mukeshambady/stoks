import "./body.css";
import { Search } from "@material-ui/icons";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { CircularProgress, TextField } from "@material-ui/core";
import { useEffect, useRef, useState } from "react";

import axios from "../../axiosCall/axios";
import StockDetails from "../stock-details/StockDetails";

function Body() {
  const [stock, setStock] = useState([]);
  const [loading, setLoading] = useState("");
  const nameRef = useRef();
  const [stockDetails, setStockDetails] = useState([]);

  function clickHandler(e) {
    let name = nameRef.current?.value || "";
    name.length > 1 &&
      axios.get(`/getDetails/${name}`).then((response) => {
        setStockDetails(response?.data);
        response?.data[0]?.Name ? setLoading(false) : setLoading(true);
      });
  }

  useEffect(() => {
    axios.get("/get").then((response) => {
      setStock(response.data);
    });
  }, []);

  return (
    <div className="container">
      <div className="title">
        <span>
          The easiest way to buy <br /> and sell Stocks
        </span>
      </div>
      <div className="mutedText">
        <p>
          Stock analysis and screening tool for <br /> invertors in India
        </p>
      </div>
      <div className="topbarCenter">
        <div className="searchbar">
          <Search className="searchIcon" />
          <Autocomplete
            className="searchInput"
            freeSolo
            options={stock?.map((option) => option.Name)}
            renderInput={(params) => (
              <TextField
                {...params}
                margin="normal"
                inputRef={nameRef}
                onSelect={clickHandler}
              />
            )}
          />
        </div>
      </div>
      {loading && <CircularProgress color="secondary" />}
      {stockDetails[0]?.Name && <StockDetails stockDetails={stockDetails} />}
    </div>
  );
}

export default Body;
